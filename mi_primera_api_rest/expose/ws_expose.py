from django.http import JsonResponse
from rest_framework.decorators import api_view
from datetime import datetime

@api_view(['GET'])
def contactos(request):
    response = { 
                'codigo': 1,
                'nombre': 'Pedro Andrés',
                'apellido-paterno': 'Castro',
                'apellido-materno': 'Villa',
                'fecha-ejecucion': datetime.now()
                }
    return JsonResponse(response, safe=False, json_dumps_params={'ensure_ascii': False})                
